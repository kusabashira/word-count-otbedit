(define (WARA-remove-newline str)
  (regexp-replace-all #/\n/ str ""))

(define (WARA-show-word-count)
  (app-status-bar-msg
    (string-length
      (WARA-remove-newline (editor-get-all-string)))))

(define WARA-worked-word-count? #f)

(define (WARA-toggle-word-count)
  (if WARA-worked-word-count?
    (app-clear-event-handler "on-cursor-moved")
    (app-set-event-handler "on-cursor-moved" WARA-show-word-count))
  (set! WARA-worked-word-count? (not WARA-worked-word-count?)))
